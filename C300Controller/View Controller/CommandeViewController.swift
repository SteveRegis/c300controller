//
//  CommandeViewController.swift
//  C300Controller
//
//  Created by Steve Regis Koalaga on 2019-01-22.
//  Copyright © 2019 Steve Regis Koalaga. All rights reserved.
//

import UIKit
import CDJoystick
import VerticalSlider

class CommandeViewController: UIViewController {

    @IBOutlet weak var JoystickView: UIView!
    @IBOutlet weak var SliderView: VerticalSlider!
    
    @IBOutlet weak var PositionLabel: UILabel!
    @IBOutlet weak var VitesseLabel: UILabel!
    
    @IBOutlet weak var TanguagePositionLabel: UILabel!
    @IBOutlet weak var RoulisPositionLabel: UILabel!
    @IBOutlet weak var LacetPositionLabel: UILabel!
    
    @IBOutlet weak var TanguageVitesseLabel: UILabel!
    @IBOutlet weak var RoulisVitesseLabel: UILabel!
    @IBOutlet weak var LacetVitesseLabel: UILabel!
    
    @IBOutlet var verticalSlider: VerticalSlider!
    
    let joystick = CDJoystick()
    var a :CGFloat = 0.0
    var b :CGFloat = 0.0
    var c : CGFloat = 0.0
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       // SliderView.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
        verticalSlider.slider.addTarget(self, action: #selector(sliderChanged), for: .valueChanged)
        setUpview()
        
    }
    

    // Setting up the view for the josystick
    func setUpview() {
        // Do any additional setup after loading the view, typically from a nib.
        joystick.frame = CGRect(x: 0, y: 0, width: 250, height: 250)
        joystick.backgroundColor = .clear
        
        joystick.substrateColor = .lightGray
        joystick.substrateBorderColor = .gray
        joystick.substrateBorderWidth = 1.0
        joystick.stickSize = CGSize(width: 100, height: 100)
        joystick.stickColor = .darkGray
        joystick.stickBorderColor = .black
        joystick.stickBorderWidth = 2.0
        joystick.fade = 0.5
        joystick.trackingHandler = { joystickData in
            // self.view.center.x += joystickData.velocity.x
            // self.view.center.y += joystickData.velocity.y
            self.a = joystickData.angle
            //self.b = joystickData.angle.y
            print("\(self.a)")
            self.b = 200.0*sin(self.a)
            self.c = 200.0*cos(self.a)
            self.PositionLabel.text! = "\(self.b)"
            self.VitesseLabel.text! = "\(self.c)"
        
            self.TanguageVitesseLabel.text! = "\(self.c)"
            self.RoulisVitesseLabel.text! = "\(self.c)"
            self.LacetVitesseLabel.text! = "\(self.c)"
        }
        JoystickView.addSubview(joystick)
    }
    
    // setting up the vertical slider
    
    @objc func sliderChanged() {
        // your code here
        self.TanguagePositionLabel.text! = "\(self.verticalSlider.value)"
        self.RoulisPositionLabel.text! = "\(self.verticalSlider.value)"
        self.LacetPositionLabel.text! = "\(self.verticalSlider.value)"
        // print(verticalSlider.value)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Setting up the view for the josystick
    
}
